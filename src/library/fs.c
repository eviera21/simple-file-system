// fs.cpp: File System

#include "sfs/fs.h"

// #include <algorithm>

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

void readDisk(struct Disk * self,int blocknum, char *data);
void writeDisk(struct Disk * self,int blocknum, char *data);
bool load_inode(size_t inode_number, Inode *node);
bool save_inode(size_t inode_number, Inode *node);
size_t allocate_free_block();
void initialize_free_block_bitmap();

// Debug file system -----------------------------------------------------------

void debug(Disk *disk) {
    Block block;
    disk->readDisk(disk, 0, block.Data);
    // Read SuperBlock
    printf("sb:\n");
    printf("    magic number %s\n", (block.Super.MagicNumber == MAGIC_NUMBER ? "valid" : "invalid"));
    printf("    %u blocks\n", block.Super.Blocks);
    printf("    %u inode blocks\n" , block.Super.InodeBlocks);
    printf("    %u inodes\n", block.Super.Inodes);
    // Read Inode blocks
    for(int i = 1; i <= block.Super.InodeBlocks; i++){
        Block i_block;
        disk->readDisk(disk, i, i_block.Data);

        for (int j = 0; j < INODES_PER_BLOCK; j++){

            if (i_block.Inodes[j].Valid == 1) {
                printf("inode %d:\n", j);
                printf("    size: %d bytes\n", i_block.Inodes[j].Size);
                printf("    direct blocks:");
                for(int m = 0; m < POINTERS_PER_INODE; m++){
                    if(i_block.Inodes[j].Direct[m]) printf(" %d", i_block.Inodes[j].Direct[m]);
                }

                if(i_block.Inodes[j].Indirect){
                    Block indirect_block;
                    disk->readDisk(disk, i_block.Inodes[j].Indirect, indirect_block.Data);
                    printf("\n    indirect block: %d\n", i_block.Inodes[j].Indirect);
                    printf("    indirect data blocks:");
                    for(int k = 0; k < POINTERS_PER_BLOCK; k++)
                        if(indirect_block.Pointers[k]) printf(" %d", indirect_block.Pointers[k]);
                }
                printf("\n");
            }
        }
    }
    
}

// Format file system ----------------------------------------------------------

bool format(Disk *disk) {
    if (disk->mounted(disk)) return false;

    // Write SuperBlock
    Block sb = {0};
    sb.Super.MagicNumber = MAGIC_NUMBER;
    sb.Super.Blocks = disk->size(disk);
    sb.Super.InodeBlocks = sb.Super.Blocks/10;
    if (sb.Super.Blocks % 10 != 0) sb.Super.InodeBlocks++;
    sb.Super.Inodes = sb.Super.InodeBlocks * INODES_PER_BLOCK;
    disk->writeDisk(disk, 0, sb.Data);

    // Clear all other blocks
    Block empty_block = {0};
    for (unsigned int i = 1; i < disk->size(disk); i++) disk->writeDisk(disk, i, empty_block.Data);

    return true;
}



// Mount file system -----------------------------------------------------------

bool mount(Disk *disk) {
    if (disk->mounted(disk)){
        return false; 
    }
    // Read SuperBlock
    Block sb;
    disk->readDisk(disk, 0, sb.Data);

    if (sb.Super.MagicNumber != MAGIC_NUMBER){
        return false;
    }

    if (sb.Super.Blocks != disk->size(disk)){
        return false;
    }

    unsigned int expected_i_blocks = sb.Super.Blocks/10;
    if (sb.Super.Blocks % 10 != 0){
        expected_i_blocks++;
    }

    if (sb.Super.InodeBlocks != expected_i_blocks){
        return false;
    }

    if (sb.Super.Inodes != expected_i_blocks * INODES_PER_BLOCK){
        return false;
    }

    // Set device and mount
    disk->mount(disk);

    // Copy metadata
    int free_block_bmap[sb.Super.InodeBlocks];
    int all_blocks, super_block_inodes, super_block_iblocks;

    mounted_disk = disk;
    all_blocks = sb.Super.Blocks;

    super_block_iblocks = sb.Super.InodeBlocks;
    super_block_inodes = sb.Super.Inodes;

    // Allocate free block bitmap
    //free_block_bmap.clear();
    for (unsigned int i = 0; i <= sb.Super.InodeBlocks; i++){
        free_block_bmap[i] = 0;
    }

    for (unsigned int i = sb.Super.InodeBlocks + 1; i < sb.Super.Blocks; i++){
        free_block_bmap[i] = 1;
    }

    for (size_t i = 1; i <= sb.Super.InodeBlocks; i++){

        Block i_block;
        disk->readDisk(disk, i, i_block.Data);

        for(unsigned int j = 0; j < INODES_PER_BLOCK; j++){
            if (!i_block.Inodes[j].Valid) continue;
            bool isIndirectBlock = true;
            for (size_t k = 0; k < POINTERS_PER_INODE; k++){
                if (i_block.Inodes[j].Direct[k] == 0){
                    isIndirectBlock = false;
                    break;
                }
                free_block_bmap[i_block.Inodes[j].Direct[k]] = 0;
            }
            if (isIndirectBlock){
                int Indirect = i_block.Inodes[j].Indirect;
                if (Indirect == 0) continue; // Sanity check
                Block IndirectBlock;
                disk->readDisk(disk, Indirect, IndirectBlock.Data);
                free_block_bmap[Indirect] = 0;
                for (size_t k = 0; k < POINTERS_PER_BLOCK; k++){
                    if (IndirectBlock.Pointers[k] != 0) free_block_bmap[IndirectBlock.Pointers[k]] = 0;
                }
            }
        }
    }
    return true;
}

// Create inode ----------------------------------------------------------------

size_t create() {
    // Locate free inode in inode table
    Block block;
    for (uint32_t i = 1; i <= sb.InodeBlocks; i++) {
        mounted_disk->readDisk(mounted_disk, i, block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) { 
            int base = (i-1) * INODES_PER_BLOCK;
            int offset = j;
            // Record inode if found
            if (!block.Inodes[j].Valid) {
                block.Inodes[j].Valid = 1;
                mounted_disk->writeDisk(mounted_disk, i, block.Data);
                return (base + offset);
            }
        }
    }
    return -1;
}

// Remove inode ----------------------------------------------------------------

bool removeInode(size_t inumber) {
    if(!mounted_disk->mounted(mounted_disk)){
        return false;
    }
      // Load inode information
    Block block;
    for (uint32_t i = 1; i <= sb.InodeBlocks; i++) {
        mounted_disk->readDisk(mounted_disk, i, block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {
            int base = (i-1) * INODES_PER_BLOCK;
            int offset = j;
            Inode inode = block.Inodes[j];
            if (inumber == base + offset) { 
                if (!block.Inodes[j].Valid) { 
                    return false; 
                }
                
                block.Inodes[j].Valid = 0;
                mounted_disk->writeDisk(mounted_disk, i, block.Data);
                // Free direct blocks
                if (inode.Direct[j]) {
                    for (uint32_t k = 0; k < POINTERS_PER_INODE; k++) { 
                        free_blocks = false;
                        block.Inodes[j].Direct[k] = 0;
                    }
                }
                // Free indirect blocks
                if (inode.Indirect) {
                    // Clear inode in inode table
                    free_blocks = false;
                    Block indirect_block;
                    mounted_disk->readDisk(mounted_disk, inode.Indirect, indirect_block.Data);
                    for (uint32_t h = 0; h < POINTERS_PER_BLOCK; h++) { 
                        if (indirect_block.Pointers[h]) {
                            free_blocks = false;
                            indirect_block.Pointers[h] = 0; 
                        }
                    }
                }
            }
        }
    }
    return true;
}

// Inode stat ------------------------------------------------------------------

size_t stat(size_t inumber) {
    if (!mounted_disk->mounted(mounted_disk)){
         return -1; 
    }
    // Load inode information
    if (inumber > superblock_inode_blocks - 1){
        return -1;
    } 
    int Blocki = (inumber / INODES_PER_BLOCK) + 1;
    unsigned int Inodei = inumber;
    while (Inodei >= INODES_PER_BLOCK){
        Inodei -= INODES_PER_BLOCK;
    }
    Block i_block;
    mounted_disk->readDisk(mounted_disk, Blocki, i_block.Data);
    if (!i_block.Inodes[Inodei].Valid) return -1;
    return i_block.Inodes[Inodei].Size;
}

// Read from inode -------------------------------------------------------------

size_t readInode(size_t inumber, char *data, size_t length, size_t offset) {
    // Load inode information
    Block i_block;
    for (int i = 1; i <= sb.InodeBlocks; i++) { 
        mounted_disk->readDisk(mounted_disk, i, i_block.Data);
        for (int j = 0; j < INODES_PER_BLOCK; j++) {
            int current_inode = (i-1) * INODES_PER_BLOCK + j;
            Inode inode = i_block.Inodes[j];
            if (inumber == current_inode) {
                if (inode.Valid) {
                    // Adjust length
                    uint32_t new_length = length;
                    if ((inode.Size - offset) < length) {
                        new_length = (inode.Size - offset);
                    }
                    // Read block and copy to data
                    Block read_block;
                    size_t i = floor(offset / BLOCK_SIZE);
                    if (i < POINTERS_PER_INODE) { 
                        mounted_disk->readDisk(mounted_disk, inode.Direct[i], read_block.Data);
                        memcpy(data, read_block.Data + (offset % BLOCK_SIZE), new_length);
                        return new_length;
                        
                    } else { 

                        if (inode.Indirect) {
                            Block indirect_block;
                            mounted_disk->readDisk(mounted_disk, inode.Indirect, indirect_block.Data);
                            mounted_disk->readDisk(mounted_disk, indirect_block.Pointers[i - POINTERS_PER_INODE], read_block.Data);
                            memcpy(data, read_block.Data + (offset % BLOCK_SIZE), new_length);
                            return new_length;
                        } else {
                            return -1;
                        }
                    }
                } else {
                    return -1;
                }
            } 
        }
    }
    return -1;
}

// Write to inode --------------------------------------------------------------

size_t writeInode(size_t inumber, char *data, size_t length, size_t offset) {
    if (!mounted_disk->mounted(mounted_disk)){
         return -1; 
    }
    // Load inode
    Block i_block;
    for (int i = 1; i <= sb.InodeBlocks; i++) {
        mounted_disk->readDisk(mounted_disk, i, i_block.Data);
        for (int j = 0; j < INODES_PER_BLOCK; j++) {
            int current_inode = (i-1) * INODES_PER_BLOCK + j; 
            Inode inode = i_block.Inodes[j];
            if (inumber == current_inode) {
                if (inode.Valid) {
                    // Adjust length
                    uint32_t new_length = length;
                    if ((inode.Size - offset) < length) {
                        new_length = (inode.Size - offset);
                    }
                    // Write block and copy to data
                    Block write_block;
                    size_t i = floor(offset / BLOCK_SIZE);
                    if (i < POINTERS_PER_INODE) {
                        mounted_disk->readDisk(mounted_disk, inode.Direct[i], write_block.Data);
                    }
                    return new_length;
                } else {
                    return -1;
                }
            }
        }
    }
    return -1;
}
